#include <getopt.h>

#include <cmath>
#include <queue>
#include <ctime>
#include <thread>
#include <random>
#include <iostream>
#include <fstream>

#include "../src/onnx_benchmark.h"
#include "../src/graph.h"
#include "../src/utils.h"

#define processor_id 0

const int NB_TASKS = 100;
const int IMAGE_HEIGHT = 224;
const int IMAGE_WIDTH = 224;
const int IMAGE_CHANNELS = 3;
const int OUTPUT_SIZE = 1000;
const bool IS_SYNC = 0;

const OrtApi* g_ort = OrtGetApiBase()->GetApi(ORT_API_VERSION);

int findAvailableRunningIndex(const std::vector<int>& is_running);
void replaceTensorData(std::vector<Tensor> &pined, const std::vector<Tensor> &new_tensor);
void generateInputTensors(int num_task, int batch, std::string &in_name, std::vector<std::vector<Tensor>> &waiting_tasks);
int starPuOnnx(std::string& path, std::string& in_name, std::string& out_name, int num_subgraph, int batch, int num_concurrency, int image_rate, std::ofstream& outputFile);
void warmup(int batch, std::string &in_name, std::string &out_name, Graph &graph, std::vector<std::vector<Tensor>> &pined_tensors);
void runFlowInferences(int image_rate, int num_task, int num_concurrency, int batch, std::string &in_name, std::string &out_name,Graph &graph, 
    std::vector<std::vector<Tensor>> &pined_tensors, std::ofstream& outputFile);

int main(int argc, char *argv[]) {
  std::cout << "ORT Version" << ORT_API_VERSION << std::endl;
  int concurrency = 0, batch = 0, subgraph = 0, image_rate = 0;
  std::string model, trace;

  static struct option long_options[] = {
    {"concurrency",    required_argument, 0, 'y'},
    {"batch",          required_argument, 0, 'b'},
    {"subgraph",       required_argument, 0, 's'},
    {"model",          required_argument, 0, 'm'},
    {"image_rate",     required_argument, 0, 'r'},
    {0, 0, 0, 0}
  };

  int opt;
  int option_index = 0;
  while ((opt = getopt_long(argc, argv, "y:b:s:m:r:t:", long_options, &option_index)) != -1) {
    switch (opt) {
      case 'y':
        concurrency = std::stoi(optarg);
        break;
      case 'b':
        batch = std::stoi(optarg);
        break;
      case 's':
        subgraph = std::stoi(optarg);
        break;
      case 'm':
        model = optarg;
        break;
      case 'r':
        image_rate = std::stoi(optarg);
        break;
      case 't':
        trace = optarg;
        break;
      default:
        std::cerr << "Usage: " << argv[0] << " --concurrency <num> --batch <num> --subgraph <num> --model <path>\n";
        return 1;
    }
  }

  std::cout << "Concurrency: " << concurrency << "\n";
  std::cout << "image rate: " << image_rate << "\n";
  std::cout << "Batch size: " << batch << "\n";
  std::cout << "Subgraph: " << subgraph << "\n";
  std::cout << "Model path: " << model << "\n";

  std::string in_name = "x";
  std::string out_name = "1158";

  // Chemin du fichier de sortie
  std::string outputFilePath = "results.txt";
  
  // Vérifier si le fichier est vide
  std::ifstream infile(outputFilePath);
  bool isEmpty = infile.peek() == std::ifstream::traits_type::eof();
  infile.close(); // Important de fermer le flux d'entrée
  
  // Ouvrir le fichier en mode append
  std::ofstream outputFile(outputFilePath, std::ios_base::app);
  if (!outputFile.is_open()) {
      std::cerr << "Erreur lors de l'ouverture du fichier de sortie." << std::endl;
      return 1;
  }
  
  if (isEmpty) {
      // Le fichier est vide, ajoutez l'entête
      outputFile << "batch;interval;througput;p5;p50;p90;p95;p99\n";
      outputFile.flush(); // Flush les données immédiatement
  }

  starPuOnnx(model, in_name, out_name, subgraph, batch, concurrency, image_rate, outputFile);

  outputFile.close();
}

bool CheckStatusAndReport(OrtStatus* status) {
  if (status) {
    std::cerr << g_ort->GetErrorMessage(status) << std::endl;
    return false;
  }
  return true;
}

int starPuOnnx(std::string& path, std::string& in_name, std::string& out_name, int num_subgraph, int batch, int num_concurrency, int image_rate, std::ofstream& outputFile)
{
  OrtThreadingOptions* tp_options;
  OrtStatus* statuss;

  statuss = g_ort->CreateThreadingOptions(&tp_options);
  if (!CheckStatusAndReport(statuss)) return -1;
  statuss = g_ort->SetGlobalSpinControl(tp_options, 0);
  if (!CheckStatusAndReport(statuss)) return -1;
  //for plafrim p100
  //statuss = g_ort->SetGlobalIntraOpThreadAffinity(tp_options, "10;11;12;13;14;15;16;17;18;19;20;21;22;23;24;25;26;27;28;29;30;31;32");
  statuss = g_ort->SetGlobalIntraOpThreadAffinity(tp_options, "1;2;3;4;5;6;7;8;9;10");
  if (!CheckStatusAndReport(statuss)) return -1;
  statuss = g_ort->SetGlobalIntraOpNumThreads(tp_options, 11);
  if (!CheckStatusAndReport(statuss)) return -1;
  statuss = g_ort->SetGlobalInterOpNumThreads(tp_options, 10);
  if (!CheckStatusAndReport(statuss)) return -1;

  Ort::Env env(tp_options, ORT_LOGGING_LEVEL_WARNING, "Inference");
  g_ort->ReleaseThreadingOptions(tp_options);

  Ort::SessionOptions sessionOptions;
  sessionOptions.DisablePerSessionThreads();

  int ret = starpu_init(NULL);
  if (ret == -ENODEV) {
    fprintf(stderr, "Aucun travailleur ne peut exécuter cette tâche.\n");
    return 0;
  }

  Graph graph(&env, path, num_subgraph);
  graph.InitializeRun(NB_TASKS, 1);
  starpu_set_limit_max_submitted_tasks(num_concurrency);
  starpu_set_limit_min_submitted_tasks(num_concurrency);

  // Pin the memory
  std::vector<std::vector<Tensor>> pined_tensors;
  std::vector<bool> status(num_concurrency);
  std::vector<int64_t> dimensions = {batch, IMAGE_CHANNELS, IMAGE_WIDTH, IMAGE_HEIGHT};
  graph.AllocateConcurrency<float>(num_concurrency, in_name, dimensions, pined_tensors);
  
  warmup(batch, in_name, out_name, graph, pined_tensors);
  std::cout<<"Onnx warmup ended\n";
  sleep(1);

  runFlowInferences(image_rate, NB_TASKS, num_concurrency, batch, in_name, out_name, graph, pined_tensors, outputFile);

  std::cout << "StarPu Shutdown" << std::endl;
  starpu_shutdown();

  std::cout << "Cuda device synchronize" << std::endl;
  cudaDeviceSynchronize();

  std::cout << "Cuda device reset" << std::endl;
  cudaDeviceReset();

  return 0;
}

void warmup(int batch, std::string &in_name, std::string &out_name, Graph &graph, std::vector<std::vector<Tensor>> &pined_tensors)
{
  std::vector<int> is_running(pined_tensors.size(), false);
  std::vector<float> data_output(batch * OUTPUT_SIZE);

  Tensor output{out_name, {batch, OUTPUT_SIZE}, data_output};
  std::vector<Tensor> outputs{output};

  std::vector<float> ints(batch * IMAGE_CHANNELS * IMAGE_HEIGHT * IMAGE_WIDTH, 150.0f);
  Tensor data{in_name, {batch, IMAGE_CHANNELS, IMAGE_HEIGHT, IMAGE_WIDTH}, ints};
  std::vector<Tensor> temp_vec{data};

  replaceTensorData(pined_tensors[0], temp_vec);

  std::vector<std::string> in_names = {in_name};
  std::vector<std::string> out_names = {out_name};
  graph.Run(batch, 0, 0, processor_id, 0, is_running[0], 1, in_names, out_names, pined_tensors[0], outputs);
}

// Fonction pour ajouter des tâches à la file d'attente (Producteur)
void submitByInterval(std::vector<std::vector<Tensor>>& waiting_tasks, int image_rate, Graph& graph, 
                  std::vector<int>& is_running, int batch, std::vector<std::string>& in_names, 
                  std::vector<std::string>& out_names, std::vector<std::vector<Tensor>>& pined_tensors, 
                  std::vector<std::vector<Tensor>>& outputs, 
                  int& task_id, int num_task) {
  for (auto& task : waiting_tasks) {
    std::this_thread::sleep_for(std::chrono::milliseconds(image_rate)); {
      task[0].start_time = starpu_timing_now();
      while(true) { //waiting for an available pined memory
        int availableIndex = findAvailableRunningIndex(is_running);
        if (availableIndex != -1) {
          is_running[availableIndex] = 1;
          replaceTensorData(pined_tensors[availableIndex], task);
          graph.Run(batch, task_id, num_task - task_id - 1, processor_id, task_id, 
                    is_running[availableIndex], IS_SYNC, in_names, out_names, 
                    pined_tensors[availableIndex], outputs[task_id]);
          break;
        }
      }
      task_id++;
    }
  }
}

void runFlowInferences(int image_rate, int num_task, int num_concurrency, int batch, std::string &in_name, std::string &out_name, Graph &graph, 
    std::vector<std::vector<Tensor>> &pined_tensors, std::ofstream &outputFile)
{  
  std::vector<int> is_running(pined_tensors.size());
  std::vector<std::vector<Tensor>> waiting_tasks;
  generateInputTensors(num_task, batch, in_name, waiting_tasks);

  std::vector<std::vector<Tensor>> outputs(num_task);
  for(int i=0; i<num_task; ++i) {
    std::vector<float> data_output(batch * OUTPUT_SIZE);
    Tensor data{out_name, {batch, OUTPUT_SIZE}, data_output};
    std::vector<Tensor> temp_vec{data};
    outputs[i] = temp_vec;
  }

  std::mutex queue_mutex;
  std::condition_variable queue_condition;
  //std::queue<std::vector<Tensor>> input_queue;

  int task_id = 0;

  std::vector<std::string> in_names = {in_name};
  std::vector<std::string> out_names = {out_name};
  auto start_gpu = std::chrono::high_resolution_clock::now();

  std::queue<std::vector<Tensor>> input_queue;

  std::thread task_adder_thread(submitByInterval, std::ref(waiting_tasks), image_rate, std::ref(graph), 
              std::ref(is_running), batch, std::ref(in_names), 
              std::ref(out_names), std::ref(pined_tensors), 
              std::ref(outputs), std::ref(task_id), num_task);

  // Attendre la fin des threads
  task_adder_thread.join();

  starpu_task_wait_for_all();
  auto end = std::chrono::high_resolution_clock::now();
  auto duration = std::chrono::duration_cast<std::chrono::nanoseconds>(end - start_gpu);
  std::cout<<"Inference duration : "<<duration.count()/num_task<< "ns Debit : " << 1.0/(duration.count()/(num_task*batch)/1e9) << std::endl;

  std::vector<double> latences = graph.GetLatences();
  for(const auto latence : latences)
    std::cout << latence << std::endl;

  std::cout << "Latence p_50 : " << Utils::Percentile(latences,0.50) << "us" << std::endl;
  std::cout << "Latence p_90 : " << Utils::Percentile(latences,0.90) << "us" << std::endl;
  std::cout << "Latence p_95 : " << Utils::Percentile(latences,0.95) << "us" << std::endl;
  std::cout << "Latence p_99 : " << Utils::Percentile(latences,0.99) << "us" << std::endl;
  outputFile << batch << ";" << image_rate << ";" << 1.0/(duration.count()/(num_task*batch)/1e9) << ";" << Utils::Percentile(latences,0.05) << ";" << Utils::Percentile(latences,0.50) << ";" << Utils::Percentile(latences,0.90) << ";" << Utils::Percentile(latences,0.95) << ";" << Utils::Percentile(latences,0.99) << "\n";
  outputFile.flush();
}

int findAvailableRunningIndex(const std::vector<int>& is_running) {
  for (size_t i = 0; i < is_running.size(); ++i) {
    if (is_running[i] == 0) {
      return i;
    }
  }
  return -1;
}

void generateInputTensors(int num_task, int batch, std::string &in_name, std::vector<std::vector<Tensor>> &waiting_tasks) {
  waiting_tasks.reserve(num_task);
  for(int i = 0; i < num_task; ++i) {
    std::vector<float> ints;
    ints.reserve(batch * IMAGE_CHANNELS * IMAGE_HEIGHT * IMAGE_WIDTH);

    std::mt19937 gen(static_cast<unsigned int>(std::time(nullptr)));
    std::uniform_real_distribution<float> dis(0.0, 255.0);
    
    for(int j = 0; j < batch * IMAGE_CHANNELS * IMAGE_HEIGHT * IMAGE_WIDTH; ++j) {
      ints.push_back(dis(gen));
    }

    Tensor data{in_name, {batch, IMAGE_CHANNELS, IMAGE_HEIGHT, IMAGE_WIDTH}, ints};
    waiting_tasks.emplace_back(std::vector<Tensor>{data});
  }
}

void replaceTensorData(std::vector<Tensor> &pined, const std::vector<Tensor> &new_tensor) {
  for(std::size_t i = 0; i < pined.size(); ++i)
  {
    memcpy(pined[i].data, new_tensor[i].data, new_tensor[i].TotalSizeInBytes());
    pined[i].start_time = new_tensor[i].start_time;
  }
}
