#include "config_parser.h"

#include <iostream>
#include <fstream>
#include <sstream>
#include <algorithm>

std::vector<int> ConfigParser::parseDims(const std::string& line) {
  std::vector<int> dims;
  std::stringstream ss(line);
  std::string temp;
  while (std::getline(ss, temp, ',')) {
    dims.push_back(std::stoi(temp));
  }
  return dims;
}

Config ConfigParser::parseConfig(const std::string& filename) {
  Config config;
  std::ifstream file(filename);
  std::string line;

  if (!file.is_open()) {
    std::cerr << "Unable to open file\n";
    return config;
  }

  while (getline(file, line)) {
    std::istringstream iss(line);
    std::string key;
    if (getline(iss, key, ':')) {
      key.erase(remove_if(key.begin(), key.end(), isspace), key.end()); // Remove whitespace
      if (key == "name" || key == "platform" || key == "max_batch_size") {
        std::string value;
        getline(iss, value);
        value.erase(remove_if(value.begin(), value.end(), isspace), value.end()); // Remove whitespace
        if (key == "name") {
          config.name = value.substr(1, value.length() - 2); // Remove quotes
        } else if (key == "platform") {
          config.platform = value.substr(1, value.length() - 2); // Remove quotes
        } else if (key == "max_batch_size") {
          config.max_batch_size = std::stoi(value);
        }
      } else if (key == "input[" || key == "output[") {
        InputOutput io;
        while (getline(file, line) && line.find('}') == std::string::npos) {
          std::istringstream field_stream(line);
          std::string field_key, field_value;
          getline(field_stream, field_key, ':');
          field_key.erase(remove_if(field_key.begin(), field_key.end(), isspace), field_key.end()); // Remove whitespace
          getline(field_stream, field_value);
          field_value.erase(remove_if(field_value.begin(), field_value.end(), isspace), field_value.end()); // Remove whitespace
          if (field_key == "name") {
            io.name = field_value.substr(1, field_value.length() - 2); // Remove quotes
          } else if (field_key == "data_type") {
            io.data_type = field_value;
          } else if (field_key == "dims") {
            io.dims = parseDims(field_value.substr(field_value.find("[") + 1, field_value.find("]") - field_value.find("[") - 1));
          }
        }
        if (key == "input[") {
          config.inputs.push_back(io);
        } else {
          config.outputs.push_back(io);
        }
      } else if (key == "instance_group[") {
        InstanceGroup ig;
        while (getline(file, line) && line.find('}') == std::string::npos) {
          std::istringstream field_stream(line);
          std::string field_key, field_value;
          getline(field_stream, field_key, ':');
          field_key.erase(remove_if(field_key.begin(), field_key.end(), isspace), field_key.end()); // Remove whitespace
          getline(field_stream, field_value);
          field_value.erase(remove_if(field_value.begin(), field_value.end(), isspace), field_value.end()); // Remove whitespace
          if (field_key == "count") {
            ig.count = std::stoi(field_value);
          } else if (field_key == "kind") {
            ig.kind = field_value.substr(0, field_value.length()); // Remove quotes
          }
        }
        config.instance_groups.push_back(ig);
      }
    }
  }

  file.close();
  return config;
}