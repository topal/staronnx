#include "tensor.h"

#include <numeric>

Tensor::Tensor(const std::string& name, ONNXTensorElementDataType type, std::vector<int64_t> dims)
    : name(name), type(type), data_size(GetDataSize(type)), dims(dims)
{
}

Tensor::Tensor(const std::string& name, const std::vector<int64_t>& dims, const std::vector<float>& vec)
    : name(name), type(ONNX_TENSOR_ELEMENT_DATA_TYPE_FLOAT), data_size(sizeof(float)), size(vec.size()), dims(dims), data(CreateData<float>(vec))
{
}

Tensor::Tensor(const std::string& name, const std::vector<int64_t>& dims, const std::vector<int64_t>& vec)
    : name(name), type(ONNX_TENSOR_ELEMENT_DATA_TYPE_INT64), data_size(sizeof(int64_t)), size(vec.size()), dims(dims), data(CreateData<int64_t>(vec))
{
}

size_t Tensor::TotalSizeInBytes() const 
{
  size_t totalSize = 0;
  totalSize += data_size * size;
  totalSize += dims.capacity() * sizeof(dims[0]);
  totalSize += name.capacity() * sizeof(name[0]);
  return totalSize;
}

size_t Tensor::GetDataSize(ONNXTensorElementDataType type) const
{
  switch (type) {
    case ONNX_TENSOR_ELEMENT_DATA_TYPE_FLOAT:
      return sizeof(float);
    case ONNX_TENSOR_ELEMENT_DATA_TYPE_INT64:
      return sizeof(int64_t);
    default:
      throw std::invalid_argument("Unknown data type");
  }
}

template<typename T>
void* Tensor::CreateData(const std::vector<T>& vec) const
{
  T* data_array = new T[vec.size()];
  std::memcpy(data_array, vec.data(), vec.size() * sizeof(T));
  return data_array;
}

size_t Tensor::InfoSizeof() const
{
  size_t size = sizeof(int64_t);
  size += sizeof(size_t);
  size += sizeof(size_t);
  size += sizeof(ONNXTensorElementDataType);
  size += this->name.size() * sizeof(name[0]);
  size += this->dims.size() * sizeof(dims[0]);
  return size;
}

TensorInfo Tensor::ToInfo()
{
  TensorInfo info{this->size, this->data_size, this->dims.size(), this->type, this->name.data(), this->dims.data()};
  return info;
}

void Tensor::RegisterOnDevice(std::vector<std::string> &out_names)
{
  bool is_output_tensor = std::find(out_names.begin(), out_names.end(), this->name) != out_names.end();
  if (this->data != nullptr && !is_output_tensor) {
    starpu_vector_data_register(&this->handle, STARPU_MAIN_RAM, (uintptr_t)this->data, this->size, this->data_size);
  } else if (this->data == nullptr) {
    starpu_vector_data_register(&this->handle, -1, (uintptr_t)NULL, this->size, this->data_size);
  }
}

void Tensor::FreeOnDevice()
{
  if (this->handle != nullptr) {
    starpu_data_unregister_submit(this->handle);
  }
}

const onnx::TypeProto* Tensor::FindTensorTypeProto(const std::string& name, const onnx::GraphProto &graph_proto)
{
  for (const auto& input : graph_proto.input()) {
    if (name == input.name()) {
      return &input.type();
    }
  }
  for (const auto& output : graph_proto.output()) {
    if (name == output.name()) {
      return &output.type();
    }
  }
  return nullptr;
}

ONNXTensorElementDataType Tensor::GetTensorType(const std::string& name, const onnx::GraphProto& graph_proto)
{
  auto type_proto = FindTensorTypeProto(name, graph_proto);
  if (type_proto != nullptr) {
    if (type_proto->has_tensor_type()) {
      switch(type_proto->tensor_type().elem_type()) {
        case onnx::TensorProto_DataType_FLOAT:
          return ONNX_TENSOR_ELEMENT_DATA_TYPE_FLOAT;
        case onnx::TensorProto_DataType_INT64:
          return ONNX_TENSOR_ELEMENT_DATA_TYPE_INT64;
        default:
          throw std::invalid_argument("Unknown tensor element type");
      }
    } else {
      throw std::invalid_argument("Input is not a tensor");
    }
  } else {
    throw std::invalid_argument("Tensor not found");
  }
}