#include "graph.h"

#include <sstream>

int convertStringToInt(const std::string& str) 
{
  try {
    return std::stoi(str);
  } catch (const std::invalid_argument& e) {
    return 0;
  } catch (const std::out_of_range& e) {
    return 0;
  }
}

char* intToCharPointer(int value) 
{
  std::ostringstream oss;
  oss << value;
  std::string str = oss.str();
  char *reslt = new char[str.length() + 1];
  std::strcpy(reslt, str.c_str());
  return reslt;
}

void Graph::Submit(int index, int priority, int worker_id, int num_inference, int &is_running, starpu_codelet &codelet, bool is_sync,
  std::vector<std::string> &in_names, std::vector<std::string> &out_names, GraphSubmit& graph_submit, 
  std::vector<Tensor *> &inputs, std::vector<Tensor *> &outputs)
{
  size_t num_buffers, layer_size = 0;
  struct LayerInfo *layer_info = InitializeLayerInformation(index, layer_size, num_buffers, inputs, outputs);
  struct CallbackInfo *callback_info = InitializeCallbackInformation(in_names[0], out_names[0], index, is_running, graph_submit, inputs, outputs);

  auto *task = starpu_task_create();
  task->name = intToCharPointer(num_inference);
  task->cl = &codelet;
  task->synchronous = is_sync;
  task->cl_arg_free = 1;
  task->cl_arg = layer_info;
  task->cl_arg_size = layer_size;
  task->callback_func = &Graph::TaskEndCallback;
  task->callback_arg = callback_info;
  task->transaction = NULL;
  task->dyn_handles = (starpu_data_handle_t*)malloc(num_buffers * sizeof(*(task->dyn_handles)));
  task->dyn_modes = (starpu_data_access_mode*)malloc(num_buffers * sizeof(*(task->dyn_modes)));

  /*
  TODO : For pheft scheduler
  uint32_t allowed_workers[1] = {0};
  allowed_workers[0] = (1 << 0) | (0 << 1) | (1 << 54);
  task->workerids = allowed_workers;
  task->workerids_len = 1;
  */

  if (task->dyn_handles == NULL || task->dyn_modes == NULL) {
    throw std::bad_alloc();
  }

  task->nbuffers = num_buffers;
  if (priority != -1) {
    task->priority = graph_submit.priority;
  }

  if (worker_id != -1) {
    task->execute_on_a_specific_worker = 1;
    task->workerid = worker_id;
  }

  //Input handle
  for (auto i = 0 ; i < layer_info->in_size; ++i) {
    task->dyn_handles[i] = inputs[i]->handle;
    task->dyn_modes[i] = STARPU_R;
  }

  //Output handle
  for (size_t i = layer_info->in_size; i < num_buffers; ++i) {
    task->dyn_handles[i] = outputs[i-layer_info->in_size]->handle;
    task->dyn_modes[i] = STARPU_W;
  }

  task->tag_id = convertStringToInt(outputs[0]->name);

  const auto ret = starpu_task_submit(task);

  STARPU_CHECK_RETURN_VALUE(ret, "starpu_task_submit");
}

void Graph::TaskEndCallback(void *args)
{
  auto worker_id = starpu_combined_worker_get_id();
  struct CallbackInfo *infos = (struct CallbackInfo*) args;

  //Free binding
  (*infos->bindings[worker_id]).ClearBoundInputs();
  (*infos->bindings[worker_id]).ClearBoundOutputs();

  for (int i=0; i<infos->out_size; ++i) {
    if (infos->out_tensors[i]->name == infos->out_name) {
      *(infos->latence) = starpu_timing_now()-*(infos->latence);
      *(infos->worker_id) = worker_id;
      *(infos->is_running) = 0;
      
      //auto it = infos->graph_submit->tensors_map.find(infos->out_name);
      //it->second.freeData();
      //auto it2 = infos->graph_submit->tensors_map.find(infos->in_name);
      //it2->second.freeData();
      //starpu_data_acquire(infos->out_tensors[i]->handle, STARPU_R);
      //const auto *scores = (const float *)starpu_data_get_local_ptr(infos->out_tensors[i]->handle);
    }
  }
}

void Graph::CudaOrtInference(void *buffers[], void *args)
{
  LayerInfo* infos = static_cast<LayerInfo*>(args);
  auto worker_id = starpu_worker_get_id();
  Ort::MemoryInfo memory("Cuda", OrtAllocatorType::OrtDeviceAllocator, worker_id, OrtMemType::OrtMemTypeDefault);

  for (auto i = 0; i < infos->in_size; ++i) {
    Ort::Value in_tensor = Ort::Value::CreateTensor(memory, reinterpret_cast<void *>STARPU_VECTOR_GET_PTR(buffers[i]), infos->in[i].size * infos->in[i].data_size,
    infos->in[i].dim_data, infos->in[i].dim_size, infos->in[i].type);
    (*infos->bindings[worker_id]).BindInput(infos->in[i].name, in_tensor);
  }

  for (auto i = 0; i < infos->out_size; ++i) { // TODO: faire la liste des tenseurs de sorties à recuperer
    Ort::Value out_tensor = Ort::Value::CreateTensor(memory, reinterpret_cast<void *>STARPU_VECTOR_GET_PTR(buffers[i+infos->in_size]), infos->out[i].size * infos->out[i].data_size,
    infos->out[i].dim_data, infos->out[i].dim_size, infos->out[i].type);
    (*infos->bindings[worker_id]).BindOutput(infos->out[i].name, out_tensor);
  }

  Ort::RunOptions run_options;
  run_options.AddConfigEntry("disable_synchronize_execution_providers", "1");
  (*infos->sessions[worker_id]).Run(run_options, (*infos->bindings[worker_id]));
}

void Graph::CpuOrtInference(void *buffers[], void *args)
{
  LayerInfo* infos = static_cast<LayerInfo*>(args);
  auto worker_id = starpu_combined_worker_get_id();
  Ort::MemoryInfo memory("Cpu", OrtAllocatorType::OrtArenaAllocator, worker_id, OrtMemType::OrtMemTypeDefault);

  for (auto i = 0; i < infos->in_size; ++i) {
    Ort::Value in_tensor = Ort::Value::CreateTensor(memory, reinterpret_cast<void *>STARPU_VECTOR_GET_PTR(buffers[i]), infos->in[i].size * infos->in[i].data_size,
    infos->in[i].dim_data, infos->in[i].dim_size, infos->in[i].type);
    (*infos->bindings[worker_id]).BindInput(infos->in[i].name, in_tensor);      
  }

  for (auto i = 0; i < infos->out_size; ++i) {
    Ort::Value out_tensor = Ort::Value::CreateTensor(memory, reinterpret_cast<void *>STARPU_VECTOR_GET_PTR(buffers[i+infos->in_size]), infos->out[i].size * infos->out[i].data_size,
    infos->out[i].dim_data, infos->out[i].dim_size, infos->out[i].type);
    (*infos->bindings[worker_id]).BindOutput(infos->out[i].name, out_tensor);
  }

  if(infos->first_worker_id == no_combined_workers) {
    (*infos->sessions[worker_id]).Run(Ort::RunOptions(), (*infos->bindings[worker_id]));
  } else {
    starpu_bind_thread_on_worker(infos->first_worker_id);
    (*infos->sessions[worker_id]).Run(Ort::RunOptions(), (*infos->bindings[worker_id]));
    starpu_bind_thread_on_worker(starpu_worker_get_id());
  }
}

CallbackInfo* Graph::InitializeCallbackInformation(std::string &in_name, std::string &out_name, int index, int &is_running, 
    GraphSubmit& graph_submit, std::vector<Tensor*>& inputs, std::vector<Tensor*>& outputs)
{
  struct CallbackInfo *callback_info = (struct CallbackInfo *)malloc(sizeof(CallbackInfo));
  if (callback_info == NULL) {
    std::cerr << "CallbackInfo memory allocation error." << std::endl;
    return NULL;
  }

  callback_info->is_running = &is_running;
  callback_info->graph_submit = &(graph_submit);
  callback_info->latence = &(this->latences_[graph_submit.id]);
  callback_info->worker_id = &(this->workers_ids_[graph_submit.id]);
  callback_info->in_size = inputs.size();
  callback_info->out_size = outputs.size();
  callback_info->in_tensors  = (Tensor**) malloc(inputs.size() * sizeof(Tensor*));
  callback_info->out_tensors  = (Tensor**) malloc(outputs.size() * sizeof(Tensor*));

  const char* out_tensor_name = out_name.c_str();
  callback_info->out_name = (char*) malloc((out_name.length() + 1) * sizeof(out_name[0]));
  strcpy(callback_info->out_name, out_tensor_name);

  const char* in_tensor_name = in_name.c_str();
  callback_info->in_name = (char*) malloc((in_name.length() + 1) * sizeof(out_name[0]));
  strcpy(callback_info->in_name, in_tensor_name);

  for (size_t i = 0; i < inputs.size(); ++i) {
    callback_info->in_tensors[i] = inputs[i];
    if(inputs[i]->name==in_name) {
      //this->latences_[graph_submit.id] = starpu_timing_now();
      this->latences_[graph_submit.id] = inputs[i]->start_time;
    }
  }

  for(size_t i = 0; i < outputs.size(); ++i) {
    callback_info->out_tensors[i] = outputs[i];
  }
  callback_info->bindings = (Ort::IoBinding**) malloc(this->bindings_.size() * sizeof(Ort::IoBinding*));
  if(callback_info->bindings == NULL) {
    std::cerr << "Bindings memory allocation error." << std::endl;
    return NULL;
  }

  for (size_t i = 0; i < this->bindings_.size(); ++i) {
    callback_info->bindings[i] = &(this->bindings_[i][index]);
  }

  return callback_info;
}

LayerInfo *Graph::InitializeLayerInformation(int index, size_t &size, size_t &num_buffers,
    std::vector<Tensor *> &inputs, std::vector<Tensor *> &outputs)
{
  struct LayerInfo *layer_info = (struct LayerInfo *)malloc(sizeof(LayerInfo));
  if(layer_info == NULL) {
    std::cerr << "LayerInfo allocation memory error." << std::endl;
    return NULL;
  }

  if(this->firsts_workers_ids_.size() > 0)
    layer_info->first_worker_id = this->firsts_workers_ids_[this->combined_idx_];
  else
    layer_info->first_worker_id = no_combined_workers;
  layer_info->in_size = inputs.size();
  layer_info->out_size = outputs.size();
  layer_info->in  = (TensorInfo*) malloc(inputs.size() * sizeof(TensorInfo));
  layer_info->out = (TensorInfo*) malloc(outputs.size() * sizeof(TensorInfo));
  layer_info->sessions = (Ort::Session**) malloc(this->sessions_.size() * sizeof(Ort::Session*));
  layer_info->bindings = (Ort::IoBinding**) malloc(this->bindings_.size() * sizeof(Ort::IoBinding*));
  for (size_t i = 0; i < this->sessions_.size(); ++i) {
    layer_info->sessions[i] = &(this->sessions_[i][index]);
  }
  for (size_t i = 0; i < this->bindings_.size(); ++i) {
    layer_info->bindings[i] = &(this->bindings_[i][index]);
  }

  size_t in_size = 0;
  for (auto &tensor : inputs) {
    size += tensor->InfoSizeof();
    layer_info->in[in_size++] = tensor->ToInfo();
  }

  size_t out_size = 0;
  for (auto &tensor : outputs) {
    size += tensor->InfoSizeof();
    layer_info->out[out_size++] = tensor->ToInfo();
  }

  num_buffers = layer_info->in_size + layer_info->out_size;
  return layer_info;
}
