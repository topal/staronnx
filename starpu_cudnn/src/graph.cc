#include "graph.h"

#include <fstream>

Graph::Graph(Ort::Env *ort_env, const std::string& path, int graph_count) : ort_env_(ort_env), path_(path), sub_graphs_(graph_count)
{
  for (int i = 0; i < graph_count; ++i) {
    this->sub_graphs_[i].name = std::to_string(i);
    this->sub_graphs_[i].key = i;
    const std::string model_path = this->path_ + this->sub_graphs_[i].name + ".onnx";
    std::ifstream model_file(model_path, std::ios::binary);
    if (!model_file.is_open()) {
      throw std::runtime_error("Failed to open model file: " + model_path);
    }

    model_file.seekg(0, std::ios::end);
    std::vector<char> buffer;
    buffer.resize(model_file.tellg());
    if (buffer.empty()) {
      throw std::runtime_error("Model file is empty: " + model_path);
    }
    model_file.seekg(0, std::ios::beg);
    model_file.read(buffer.data(), buffer.size());
    model_file.close();

    onnx::ModelProto model_proto;
    bool parse_result = model_proto.ParseFromArray(buffer.data(), buffer.size());
    if (!parse_result) {
      throw std::runtime_error("Failed to parse model from file: " + model_path);
    }
    const auto& onnx_graph = model_proto.graph();

    BuildSubGraphFromOnnx(i, onnx_graph);
  }
}

void Graph::BuildSubGraphFromOnnx(int index, const onnx::GraphProto& graph_proto)
{
  Ort::AllocatorWithDefaultOptions allocator;
  Ort::SessionOptions session_options;
  session_options.SetIntraOpNumThreads(1);
  std::string model = this->path_ + this->sub_graphs_[index].name + ".onnx";
  Ort::Session session(*ort_env_, model.c_str(), session_options);

  //Add to the map the inputs tensors from the first nodes
  for (size_t i = 0; i < session.GetInputCount(); ++i) {
    Ort::TypeInfo type_info = session.GetInputTypeInfo(i);
    auto tensor_info = type_info.GetTensorTypeAndShapeInfo();
    std::vector<int64_t> shape = tensor_info.GetShape();

    Ort::AllocatedStringPtr input_name = session.GetInputNameAllocated(i, allocator);
    std::string name = input_name.get();
    Tensor tensor(name, Tensor::GetTensorType(name, graph_proto), shape);

    auto inserted_tensor = this->tensors_.emplace(tensor.name, tensor);    
    this->sub_graphs_[index].inputs.push_back(&inserted_tensor.first->second);
  }

  //Add to the map the outputs tensors from the last nodes  
  for (size_t i = 0; i < session.GetOutputCount(); ++i) {
    Ort::TypeInfo type_info = session.GetOutputTypeInfo(i);
    auto tensor_info = type_info.GetTensorTypeAndShapeInfo();
    std::vector<int64_t> shape = tensor_info.GetShape();

    Ort::AllocatedStringPtr output_name = session.GetOutputNameAllocated(i, allocator);
    std::string name = output_name.get();
    Tensor tensor(name, Tensor::GetTensorType(name, graph_proto), shape);

    auto inserted_tensor = this->tensors_.emplace(tensor.name, tensor);  
    this->sub_graphs_[index].outputs.push_back(&inserted_tensor.first->second);
  }
}

void Graph::InitializeSessions(void *arg)
{
  auto* session_args = static_cast<SessionArg*>(arg);
  const int worker_id = starpu_worker_get_id();
  Ort::SessionOptions session_options;

  //Gpu Worker
  if (starpu_worker_get_type(worker_id) == STARPU_CUDA_WORKER) {
    session_options.SetIntraOpNumThreads(1);
    OrtCUDAProviderOptions cuda_options;
    cuda_options.device_id = starpu_worker_get_devid(worker_id);
    cuda_options.has_user_compute_stream = true;
    cuda_options.user_compute_stream = starpu_cuda_get_local_stream();
    session_options.AppendExecutionProvider_CUDA(cuda_options);
    for (auto &sub_graph : *session_args->sub_graphs) {
      std::string path = session_args->path+sub_graph.name+".onnx";
      (*session_args->sessions)[worker_id].emplace_back(Ort::Session((*session_args->ort_env), path.c_str(), session_options));
      (*session_args->idx_sessions)[sub_graph.key] = (*session_args->sessions)[worker_id].size()-1;
      (*session_args->bindings)[worker_id].emplace_back(Ort::IoBinding((*session_args->sessions)[worker_id].back()));
      (*session_args->idx_bindings)[sub_graph.key] = (*session_args->bindings)[worker_id].size()-1;
    }
  }
  //Cpu Worker
  else if (starpu_worker_get_type(worker_id) == STARPU_CPU_WORKER) {
    session_options.SetIntraOpNumThreads(1);
    for(auto &sub_graph: *session_args->sub_graphs) {
      std::string path = session_args->path+sub_graph.name+".onnx";
      (*session_args->sessions)[worker_id].emplace_back(Ort::Session((*session_args->ort_env), path.c_str(), session_options));
      (*session_args->idx_sessions)[sub_graph.key] = (*session_args->sessions)[worker_id].size()-1;
      (*session_args->bindings)[worker_id].emplace_back(Ort::IoBinding((*session_args->sessions)[worker_id].back()));
      (*session_args->idx_bindings)[sub_graph.key] = (*session_args->bindings)[worker_id].size()-1;
    }
  } else {
    throw std::runtime_error("Unsupported worker type!");
  }
}

void Graph::InitializeSessions(std::vector<int> combined_worker_ids, void *arg)
{
  for (const auto combined_id : combined_worker_ids) {
    auto* session_args = static_cast<SessionArg*>(arg);
    const int worker_id = combined_id;

    Ort::SessionOptions session_options;
    session_options.DisablePerSessionThreads();
    for (auto &sub_graph : *session_args->sub_graphs) {
      std::string path = session_args->path+sub_graph.name+".onnx";
      (*session_args->sessions)[worker_id].emplace_back(Ort::Session((*session_args->ort_env), path.c_str(), session_options));
      (*session_args->idx_sessions)[sub_graph.key] = (*session_args->sessions)[worker_id].size()-1;
      (*session_args->bindings)[worker_id].emplace_back(Ort::IoBinding((*session_args->sessions)[worker_id].back()));
      (*session_args->idx_bindings)[sub_graph.key] = (*session_args->bindings)[worker_id].size()-1;
    }
  }
}

void Graph::CreateCombinedWorkers(int num_combined_workers)
{
  int total_cpu_workers = starpu_worker_get_count_by_type(STARPU_CPU_WORKER);
  if (total_cpu_workers < num_combined_workers) {
    std::cerr << "Invalid number of combined workers." << std::endl;
    return;
  }

  std::vector<int> workers_ids_(total_cpu_workers);
  int ret = starpu_worker_get_ids_by_type(STARPU_CPU_WORKER, workers_ids_.data(), total_cpu_workers);

  if (ret >= 0) {
    combined_workers_ids.clear();
    firsts_workers_ids_.clear();
    int start_idx = 0;

    int workers_per_combined = total_cpu_workers / num_combined_workers;
    int extra_workers = total_cpu_workers % num_combined_workers;
    for (int i = 0; i < num_combined_workers; ++i) {
      int num_workers_this_combined = workers_per_combined + (i < extra_workers ? 1 : 0);
      int combined_worker_id = starpu_combined_worker_assign_workerid(num_workers_this_combined, &workers_ids_[start_idx]);
      combined_workers_ids.push_back(combined_worker_id);
      firsts_workers_ids_.push_back(workers_ids_[start_idx]);

      std::cout << "Combined Worker ID: " << combined_worker_id << " [ ";
      for (int j = start_idx; j < start_idx + num_workers_this_combined; ++j) {
        std::cout << workers_ids_[j] << " ";
      }
      std::cout << "]" << std::endl;
      start_idx += num_workers_this_combined;
    }
  } else if (ret == -ERANGE) {
    std::cerr << "Error: The provided array size is smaller than the number of workers." << std::endl;
  } else {
    std::cerr << "An error occurred while getting worker IDs." << std::endl;
  }
}

// TODO: combined CPU worker
void Graph::InitializeRun(int latence_size, int nb_combined_workers_)
{
  if(nb_combined_workers_ > 0)
    starpu_parallel_worker_init(HWLOC_OBJ_SOCKET, STARPU_PARALLEL_WORKER_TYPE, STARPU_PARALLEL_WORKER_GNU_OPENMP_MKL, 0);

  this->nb_combined_workers_ = nb_combined_workers_;
  this->latences_.resize(latence_size);
  this->workers_ids_.resize(latence_size);

  int worker_count = starpu_worker_get_count();
  if (worker_count < 0) {
    throw std::runtime_error("Error calling starpu_worker_get_count()");
  }

  if(nb_combined_workers_ > 0) {
    // TODO : Find a solution because combined workers do not necessarily have a consistent id with differents schedulers
    this->sessions_.resize(worker_count+/*this->nb_combined_workers_+*/combined_worker_limit_id);
    this->bindings_.resize(worker_count+/*this->nb_combined_workers_+*/combined_worker_limit_id);
    CreateCombinedWorkers(this->nb_combined_workers_);
  } else {
    this->sessions_.resize(worker_count);
    this->bindings_.resize(worker_count);
  }

  SessionArg args = {this->path_, this->ort_env_, &this->sub_graphs_, &this->idx_sessions_, &this->idx_bindings_, &this->sessions_, &this->bindings_};
  starpu_execute_on_each_worker(&Graph::InitializeSessions, &args, STARPU_CPU|STARPU_CUDA);

  Graph::InitializeSessions(this->combined_workers_ids, &args);

  //One starPu codelet per sub graph
  for (const auto &sub_graph : sub_graphs_) {
    struct starpu_perfmodel ort_model = {
      .type = STARPU_HISTORY_BASED,
      .symbol = sub_graph.name.c_str(),
    };
    this->perf_models_.emplace(sub_graph.name, ort_model);
    this->codelets_.emplace(sub_graph.key, starpu_codelet{
      .type = STARPU_FORKJOIN,
      .max_parallelism = INT_MAX,
      .cpu_funcs = {&Graph::CpuOrtInference},
      .cuda_funcs = {&Graph::CudaOrtInference},
      .cuda_flags = {STARPU_CUDA_ASYNC},
      .nbuffers = STARPU_VARIABLE_NBUFFERS,
      .model = &perf_models_[sub_graph.name],
      .name = sub_graph.name.c_str(),
    });
  }
}

std::vector<double> Graph::GetLatences()
{
  return this->latences_;
}

std::vector<int> Graph::GetWorkerIds()
{
  return this->workers_ids_;
}

void Graph::FreeGraph()
{
  this->graph_submits_.clear();
}

void Graph::UpdateTensorData(const std::vector<Tensor>& tensors, std::unordered_map<std::string, Tensor>& tensors_map) 
{
  for(const auto& tensor : tensors) {
    auto tensor_iter = tensors_map.find(tensor.name);
    if (tensor_iter == tensors_map.end()) {
      throw std::runtime_error("Tensor '" + tensor.name + "' not found in the map !");
    }
    tensor_iter->second = tensor;
  }
}

std::vector<Tensor*> Graph::CreateSubGraphTensors(const std::vector<Tensor*>& tensors, std::unordered_map<std::string, Tensor>& tensors_map)
{
  std::vector<Tensor*> subgraph_tensors;
  subgraph_tensors.reserve(tensors.size());
  for (const auto& tensor : tensors) {
    auto tensor_iter = tensors_map.find(tensor->name);
    if(tensor_iter == tensors_map.end()) {
      throw std::runtime_error("Tensor '" + tensor->name + "' for subgraph not found in the map!");
    }                                                           
    subgraph_tensors.emplace_back(&(tensor_iter->second));
  }
  return subgraph_tensors;
}

int itest=0;
void Graph::Run(int batch, int task_id, int priority, int worker_id, int inference_num, int &is_running, bool is_sync, 
    std::vector<std::string> &in_names, std::vector<std::string> &out_names, std::vector<Tensor>& in_tensors, 
    std::vector<Tensor>& out_tensors)
{
  this->graph_submits_.emplace_back(task_id, priority, this->tensors_);
  auto &current_graph_submit = this->graph_submits_.back();

  // Updating input tensor data in the current graph submission
  UpdateTensorData(in_tensors, current_graph_submit.tensors_map);

  // Updating output tensor data in the current graph submission and setting its data pointer to null
  for (auto& output : out_tensors) {
    output.data = nullptr;
  }
  UpdateTensorData(out_tensors, current_graph_submit.tensors_map);
  
  // Registering all tensors in the current graph submission on the device
  for (auto& [name, tensor] : current_graph_submit.tensors_map) {
    // Update the tensor size with the new batch
    tensor.size = 1;
    if (tensor.dims[0] == -1)
      tensor.dims[0] = batch;
    for (std::size_t i = 0; i < tensor.dims.size(); ++i)
      tensor.size *= tensor.dims[i];
    tensor.RegisterOnDevice(out_names);
  }

  // Processing each subgraph
  for (const auto& sub_graph : this->sub_graphs_) {      
    auto sub_graph_inputs = CreateSubGraphTensors(sub_graph.inputs, current_graph_submit.tensors_map);
    auto sub_graph_outputs = CreateSubGraphTensors(sub_graph.outputs, current_graph_submit.tensors_map);

    Submit(this->idx_sessions_[sub_graph.key], priority, worker_id, inference_num, is_running, this->codelets_[sub_graph.key], is_sync, 
      in_names, out_names, current_graph_submit, sub_graph_inputs, sub_graph_outputs);
  }

  for (auto &[name, tensor] : current_graph_submit.tensors_map) {
    tensor.FreeOnDevice();
  }
}