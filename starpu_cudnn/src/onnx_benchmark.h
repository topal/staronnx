#ifndef ONNX_BENCHMARK_HPP
#define ONNX_BENCHMARK_HPP

#include <vector>
#include <string>

void onnx_benchmark_cpu(std::string &file_path, std::string &model_path, std::string &input_name, std::string &output_name, int batch);
void onnx_benchmark_gpu(std::vector<long long> &benchmark_results, std::string &model_path, std::string &input_name, std::string &output_name, int device_id, int batch);

#endif