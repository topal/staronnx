#ifndef UTILS_H
#define UTILS_H

#include <sys/sysinfo.h>

#include <vector>
#include <stdexcept>

class VectorUtils {
 public:
  template <typename T>
  static T FindSmallest(const std::vector<T>& vec)
  {
    return *std::min_element(vec.begin(), vec.end());
  }

  template <typename T>
  static T CalculateMedian(std::vector<T> vec) 
  {
    std::sort(vec.begin(), vec.end());
    size_t size = vec.size();
    if (size % 2 == 0) {
      return (vec[size / 2 - 1] + vec[size / 2]) / 2.0;
    } else {
      return vec[size / 2];
    }
  }
};

class Utils {
 public:
  template <typename T>
  static T Percentile(const std::vector<T>& data, double p)
  {
    if (data.empty()) {
      throw std::invalid_argument("Data cannot be empty");
    }    
    if (p < 0 || p > 1) {
      throw std::invalid_argument("Percentile must be between 0 and 1");
    }
    
    std::vector<T> sorted_data = data;
    std::sort(sorted_data.begin(), sorted_data.end());
    
    double index = p * (sorted_data.size() - 1);
    int64_t int_part = static_cast<int64_t>(index);
    double frac_part = index - int_part;
    
    if (static_cast<std::size_t>(int_part + 1) < sorted_data.size()) {
      return sorted_data[int_part] * (1 - frac_part) + sorted_data[int_part + 1] * frac_part;
    } else {
      return sorted_data[int_part];
    }
  }
};

#endif // UTILS_H