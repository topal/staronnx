import onnx
import argparse
from onnx import shape_inference

def set_dynamic_batch_size(model_path, output_model_path):
    # Charger le modèle ONNX
    model = onnx.load(model_path)

    # Modifier la dimension de batch des entrées en taille dynamique
    for input in model.graph.input:
        input.type.tensor_type.shape.dim[0].dim_param = 'dynamic_batch_size'

    # Modifier la dimension de batch des sorties en taille dynamique
    for output in model.graph.output:
        output.type.tensor_type.shape.dim[0].dim_param = 'dynamic_batch_size'

    # Appliquer l'inférence de shape pour mettre à jour le modèle
    model = shape_inference.infer_shapes(model)

    # Sauvegarder le modèle modifié
    onnx.save(model, output_model_path)

def main():
    parser = argparse.ArgumentParser(description='Set the batch size of an ONNX model to dynamic.')
    parser.add_argument('model_path', type=str, help='Path to the ONNX model file.')
    parser.add_argument('output_model_path', type=str, help='Path where the modified model will be saved.')

    args = parser.parse_args()

    set_dynamic_batch_size(args.model_path, args.output_model_path)

if __name__ == "__main__":
    main()